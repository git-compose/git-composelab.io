// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use std::path::Path;

fn main() {
    if !Path::new("static/font-awesome").exists() {
        seed_icons_gen::get_fa_resources("static");
    }
}
