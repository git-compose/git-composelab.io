# Compose for Git Site

This repository contains sources for Compose for Git site to be
rendered. 
Read more about Compose for Git in [documentation](https://git-compose.gitlab.io/git-compose-docs).
