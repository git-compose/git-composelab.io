// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

pub use crate::git_compose_site::*;
pub use crate::release::*;
use std::fmt;
use seed::{prelude::*};

const ABOUT: &str = "about";
const INSTALL: &str = "install";

#[derive(Clone)]
pub struct Model {
   pub page: Page,
   pub release: Option<Release>,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Page {
    Home,
    About,
    Install,
}

impl fmt::Display for Page {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

// impl Clone for Model {
//     fn clone(&self) -> Model {
//         Model {
//             page: self.page.clone(),
//             release: self.release.clone(),
//         }
//     }
// }

impl From<Url> for Page {
    fn from(mut url: Url) -> Self {
        match url.remaining_path_parts().as_slice() {
            [ABOUT] => Self::About,
            [INSTALL] => Self::Install,
            _ => Self::Home,
        }
    }
}

pub fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::UrlChanged(subs::UrlChanged(url)) => {
            (*model).page = Page::from(url);
        },
        // Msg::Redirect(set_page) => {
        //     (*model).page = set_page;
        // },
        Msg::ReleaseReceived(set_release) => {
            (*model).release = Some(set_release);
        }
    }
}
