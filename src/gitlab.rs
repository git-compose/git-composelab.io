// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Response};

use crate::git_compose_site::*;
use crate::release::*;


#[derive(Debug)]
pub enum GetReleaseError {
    JsError(JsValue),
    NotFound(),
}

impl std::convert::From<JsValue> for GetReleaseError {
    fn from(error: JsValue) -> Self {
        GetReleaseError::JsError(error)
    }
}

async fn get_last_release(project: String) -> Result<Release, GetReleaseError> {
    let mut opts = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);

    let url = format!(
        "https://gitlab.com/api/v4/projects/{}/releases?per_page=1",
        project
    );

    let request = Request::new_with_str_and_init(&url, &opts)?;

    request.headers().set("Accept", "application/json")?;

    let window = web_sys::window().unwrap();
    let resp_value = JsFuture::from(window.fetch_with_request(&request)).await?;

    // `resp_value` is a `Response` object.
    assert!(resp_value.is_instance_of::<Response>());
    let resp: Response = resp_value.dyn_into().unwrap();

    // Convert this other `Promise` into a rust `Future`.
    let json = JsFuture::from(resp.json()?).await?;

    // Use serde to parse the JSON into a struct.
    let releases: Vec<Release> = json.into_serde().unwrap();
    let last = releases.first(); // expected only one and sorted descending
    match last {
        Some(rel) => Ok((*rel).clone()),
        None => Err(GetReleaseError::NotFound()),
    }
}

pub async fn get_release() -> Msg {
    let release = get_last_release(String::from("17475748")).await.unwrap();
    Msg::ReleaseReceived(release)
    // log!(release);

}
