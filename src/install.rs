#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use crate::release::*;
use seed::{prelude::*, *};

fn url(os_id: &str, tag_name: String, assets: Assets) -> String {
    let name = format!("gico_{}_{}", os_id, tag_name);
    let maybe_os: Option<Link> = assets.links.into_iter().find(|r| r.name == name);
    match maybe_os {
        Some(os) => os.url,
        None => String::default(),
    }
}

pub fn install(release: Release) -> Node<Msg> {
    let tag_name = release.tag_name;
    let assets = release.assets;
    let linux_url = url("linux", tag_name.clone(), assets.clone());
    let win_url = url("win", tag_name.clone(), assets.clone());
    let mac_url = url("mac", tag_name.clone(), assets.clone());
    div![
        main![div![
            C!["centered-container", "install-content"],
            Node::from_markdown(&format!(
                r#"
### With binary 

Just download prebuilt binary for your OS and add to PATH:

| Linux | Windows | MacOs |
|-------|---------|-------|
| [gico {}]({}) | [gico {}]({}) | [gico {}]({}) |

### With aptitude

There's a `.deb` package available from custom repository, you can 
read more about it [here](https://gitlab.com/git-compose/git-compose-debian).


"#,
                tag_name, linux_url, tag_name, win_url, tag_name, mac_url
            )),
            code_block(
                "bash",
                r#"sudo bash -c 'echo "deb [trusted=yes] https://git-compose.gitlab.io/git-compose-debian trusty main" > /etc/apt/sources.list.d/git-compose.list'
sudo apt-get update
sudo apt-get install git-compose
gico version"#,
                "code-panel",
            ),
            Node::from_markdown(
                r#"
### With Golang >= 1.12
"#
            ),
            code_block(
                "bash",
                "go get -u gitlab.com/git-compose/git-compose/cmd/gico",
                "code-panel",
            ),
        ]],
        div![
            C!["centered-container"],
            Node::from_markdown(
                r#"
## Auto Completions

Since gico 0.6.0

### Bash

#### With bash-completion

``` bash
gico completion bash > gico
chmod +x gico
sudo mv gico /usr/share/bash-completion/completions/gico
```

#### Manually via `.bashrc`

Add `source <(gico completion bash)` to your `.bashrc` like this:

``` bash
echo 'source <(gico completion bash)' >> ~/.bashrc
```

### Zsh

#### With oh-my-zsh

``` bash
mkdir -p $ZSH_CUSTOM/plugins/gico
gico completion zsh > $ZSH_CUSTOM/plugins/gico/gico.plugin.zsh
```

Now you need to add `gico` to the list of enabled plugins in `~/.zshrc`

"#
            )
        ]
    ]
}
