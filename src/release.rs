// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Release {
    pub name: String,
    pub tag_name: String,
    pub assets: Assets,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Assets {
    pub links: Vec<Link>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Link {
    pub name: String,
    pub url: String,
}

// #[derive(Debug)]
// pub enum GetReleaseError {
//     HttpError(attohttpc::Error),
//     Utf8Error(std::str::Utf8Error),
// }

// impl std::convert::From<attohttpc::Error> for GetReleaseError {
//     fn from(error: attohttpc::Error) -> Self {
//         GetReleaseError::HttpError(error)
//     }
// }

// impl std::convert::From<std::str::Utf8Error> for GetReleaseError {
//     fn from(error: std::str::Utf8Error) -> Self {
//         GetReleaseError::Utf8Error(error)
//     }
// }

// pub fn get_release() -> Result <(), GetReleaseError> {
//     let url =  "https://gitlab.com/api/v4/projects/17475748/releases";
//     let resp = attohttpc::get(url).send()?;
//     let body = resp.bytes()?;
//     let str_body = std::str::from_utf8(&body)?;
//     println!("{}", str_body);
//     Ok(())
// }

pub fn links() {
    // comms::run();
    // get_release().unwrap();
}


