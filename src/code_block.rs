#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.


use crate::git_compose_site::Msg;
use seed::{prelude::*, *};

pub fn code_block(lang: &str, code: &str, class: &str) -> Node<Msg> {
    custom![
        Tag::from("code-block"),
        attrs! {
            At::from("lang") => lang,
            At::from("code") => code,
            At::from("classname") => class,
        }
    ]
}
