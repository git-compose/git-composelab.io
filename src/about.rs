#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use seed::{prelude::*, *};

pub fn about() -> Node<Msg> {
    div![
        main![div![
            C!["content-grid", "centered-container"],
            div![
                C!["content-left"],
                div![
                    C!["code-header"],
                    "Example ",
                    strong!["git-compose.yaml"],
                    " file"
                ],
                code_block(
                    "yaml",
                    r#"version: "0.1"
repos:
  repo1: 
    remote: https://repo1.git
  repo2: 
    remote: https://repo2.git
  repo3: 
    remote: https://repo3.git"#,
                    "code-panel"
                ),
                Node::from_markdown(
                    "
CLI tool gico executes git on each of your local repositories
trying to reach desired state, described in **git-compose.yaml** file.

Tool is completely open source and free, licensed under BSD-type.

Whenever content of your git-compose.yaml file changes, all you need
to do is to run `gico up` command, which will firstly determine current
state of subfolders under current directory (checked out branches), 
and then will try to execute a set of `git` commands to make sure
all repos are up-to-date.
"
                ),
            ],
            div![
                C!["content-right"],
                img![attrs! {
                    At::Src => "/static/images/about.svg",
                }],
                p![
                    code_block("bash", "gico up", "inlinecode"),
                    "will",
                    Node::from_markdown(
                        "
- `pull` repositories, which are already on correct branches
- `checkout` branch if **git-compose.yaml** specified different branch
- `clone` repositories missing from current dirrectory
                "
                    )
                ]
            ],
        ]],
        section![
            C!["centered-container"],
            img![
                C!["about-yaml"],
                attrs! {
                    At::Src => "/static/images/about-yaml.svg",
                }
            ],
        ]
    ]
}
