#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

mod about;
mod code_block;
mod git_compose_site;
mod gitlab;
mod header;
mod home;
mod install;
mod log;
mod model;
mod release;

pub use crate::about::*;
pub use crate::code_block::*;
pub use crate::git_compose_site::*;
pub use crate::header::*;
pub use crate::home::*;
pub use crate::install::*;
pub use crate::model::*;
pub use crate::release::*;

use seed::{prelude::*, *};

fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.perform_cmd(gitlab::get_release());
    orders.subscribe(Msg::UrlChanged);
    Model {
        page: Page::from(url),
        release: None,
    }
}

// #[wasm_bindgen]
// extern "C" {
//     fn init_highlight();
// }

// ------ ------
//     View
// ------ ------

// (Remove the line below once your `Model` become more complex.)
#[allow(clippy::trivially_copy_pass_by_ref)]
// `view` describes what to display.
fn view(model: &Model) -> Node<Msg> {
    div![
        header(model.page.clone().to_string(), model.release.clone()),
        match model.page {
            Page::Home => home(),
            Page::About => about(),
            Page::Install => match model.release.clone() {
                Some(rel) => install(rel),
                None => div![],
            }
        },
        footer![
            C!["centered-container", "footer"],
            div![
                "This site is powered by awesome ",
                span![a![
                    C!["footer-link"],
                    "Seed",
                    attrs! {
                        At::Href => "https://seed-rs.org/",
                    }
                ]]
            ]
        ],
    ]
}

// ------ ------
//     Start
// ------ ------
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
    // init_highlight();
}
