// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use seed::{prelude::*, *};

use crate::home::feature;
use crate::home::feature::FeaturePos;

pub fn render() -> Node<Msg> {
    feature::render(
        FeaturePos::RIGHT,
        div![
            div![
                C!["code-header", "code-header-darker"],
                "Example ",
                strong!["git-compose.yaml"],
                " with remote alternatives"
            ],
            code_block(
                "yaml",
                r#"version: "0.1"
repos:
  my-repo:
    remotes:
      https: https://gitlab.com/my/repo.git
      ssh: git@gitlab.com:my/repo.git"#,
                "code-panel"
            ),
        ],
        div!(a![
            C!["header-link"],
            "Remote alternatives",
            attrs! {
                At::Href => "https://git-compose.gitlab.io/git-compose-docs/file.html#remote-alternatives",
            }
        ],),
        vec![
            span!["Give developers a choice of their preferred transport protocol"],
            span!["Specify both alternative urls: https or ssh"],
            span![
                "Choose one of them using",
                a![
                    C!["header-link"],
                    "CLI options",
                    attrs! {
                        At::Href => "https://git-compose.gitlab.io/git-compose-docs/commands.html",
                    }
                ],
                "or",
                a![
                    C!["header-link"],
                    "persisted configuration",
                    attrs! {
                        At::Href => "https://git-compose.gitlab.io/git-compose-docs/commands.html#gico-config",
                    }
                ],
            ],
        ],
    )
}
