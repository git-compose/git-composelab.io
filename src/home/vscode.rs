// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::git_compose_site::*;
use seed::{prelude::*, *};

use crate::home::feature;
use crate::home::feature::FeaturePos;

pub fn render() -> Node<Msg> {
    feature::render(
        FeaturePos::LEFT,
        img![
            C!["image-w100"],
            attrs! {
                At::Src => "/static/images/git-compose-vscode.png",
            }
        ],
        div![
            "Also for",
            a![
                C!["header-link"],
                "VSCode",
                attrs! {
                    At::Href => "https://marketplace.visualstudio.com/items?itemName=git-compose.git-compose",
                }
            ],
        ],
        vec![
            span!["VSCode Extension to use Compose for Git from inside of your favorite IDE!",],
            span!["Code Lens would provide you with most used commands",],
            span!["Built-in help popups appear with info about file syntax",],
        ],
    )
}

// section![
//     C!["centered-container", "lfeature-section-grid"],
//     div![
//         C!["feature-section-left"],
//         img![
//             C!["image-w100"],
//             attrs! {
//                 At::Src => "static/images/git-compose-vscode.png",
//             }
//         ],
//     ],
//     div![
//         C!["feature-section-right"],
//         h3![
//             C!["centertext"],
//             "Also for",
//             a![
//                 C!["header-link"],
//                 "VSCode",
//                 attrs! {
//                     At::Href => "https://marketplace.visualstudio.com/items?itemName=git-compose.git-compose",
//                 }
//             ],
//         ],
//         p![
//             green_check(),
//             "VSCode Extension to use Compose for Git from inside of your favorite IDE!",
//         ],
//         p![
//             green_check(),
//             "Code Lens would provide you with most used commands",
//         ],
//         p![
//             green_check(),
//             "Built-in help popups appear with info about file syntax",
//         ],
//     ],
// ],
