// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use seed::{prelude::*, *};

use crate::home::feature;
use crate::home::feature::FeaturePos;

pub fn render() -> Node<Msg> {
    feature::render(
        FeaturePos::LEFT,
        div![
            C!["left-code"],
            div![
                C!["code-header", "code-header-darker"],
                "Example ",
                strong!["git-compose.yaml"],
                " with inline hook"
            ],
            code_block(
                "yaml",
                r#"version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
    hooks:
      pre-push:
        script: |
          #!/bin/bash
          echo pushing"#,
                "code-panel"
            ),
        ],
        div!(
            span!["Describe"],
            a![
                C!["header-link"],
                "Git Hooks",
                attrs! {
                    At::Href => "https://git-compose.gitlab.io/git-compose-docs/file.html#hooks",
                }
            ],
        ),
        vec![
            span!["Make sure new developers easily onboard dev environments with all the necessary hooks"],
            span![
                a![
                "Inline hooks",
                attrs! {
                    At::Href => "https://git-compose.gitlab.io/git-compose-docs/file.html#hooks",
                }
                ],
                " described directly in git-compose.yaml file",
            ],
            span![
                a![
                    "File path hooks",
                    attrs! {
                        At::Href => "https://git-compose.gitlab.io/git-compose-docs/file.html#hooks",
                    }
                ],
                " to take the script from the separate file",
            ]
        ]
    )
}
