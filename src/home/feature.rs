// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::git_compose_site::*;
use seed::{prelude::*, *};
use seed_icons::fa::regular::check_circle;

fn green_check<T>() -> Node<T> {
    check_circle::i_c(vec!["green-fa", "inline-icon"])
}

#[derive(Clone, Copy)]
pub enum FeaturePos {
    LEFT,
    RIGHT,
}

fn feature_section_grid_class(pos: FeaturePos) -> &'static str {
    match pos {
        FeaturePos::LEFT => "lfeature-section-grid",
        FeaturePos::RIGHT => "rfeature-section-grid",
    }
}

fn feature_section_item_class(pos: FeaturePos) -> &'static str {
    match pos {
        FeaturePos::LEFT => "feature-section-left",
        FeaturePos::RIGHT => "feature-section-right",
    }
}

fn feature_section_desc_class(pos: FeaturePos) -> &'static str {
    match pos {
        FeaturePos::LEFT => "feature-section-right",
        FeaturePos::RIGHT => "feature-section-left",
    }
}

pub fn render(
    pos: FeaturePos,
    pic: Node<Msg>,
    title: Node<Msg>,
    items: Vec<Node<Msg>>,
) -> Node<Msg> {
    section![
        C!["centered-container", feature_section_grid_class(pos)],
        div![
            C![feature_section_item_class(pos), "feature-section-first"],
            pic
        ],
        div![
            C![feature_section_desc_class(pos), "feature-section-last"],
            h3![C!["centertext"], title],
            items
                .iter()
                .map(|e| { p![green_check(), e] })
                .collect::<Vec<_>>(),
        ],
    ]
}
