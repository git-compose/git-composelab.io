// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use seed::{prelude::*, *};

use crate::home::feature;
use crate::home::feature::FeaturePos;
pub fn render() -> Node<Msg> {
    feature::render(
        FeaturePos::RIGHT,
        img![
            C!["image-w100"],
            attrs! {
                At::Src => "/static/images/gico-init.gif",
            }
        ],
        p!["Start with", code_block("bash", "gico init", "inlinecode")],
        vec![
            span![
                "Run",
                code_block("bash", "gico init", "inlinecode"),
                "in the folder with several repos and it will
                        generate git-compose.yaml for you"
            ],
            span![
                "With url argument like this:",
                code_block("bash", "gico init <url>", "inlinecode"),
                "will download git-compose.yaml file from external sources and clone
                        all the repos. It's handy, when you want to give a command
                        for a new developers to quickly rollout dev environment."
            ],
        ],
    )
}
// section![
//     C!["centered-container", "rfeature-section-grid"],
//     div![
//         C!["feature-section-left"],
//         h3![
//             C!["centertext"],
//             p!["Start with", code_block("bash", "gico init", "inlinecode")]
//         ],
//         p![
//             green_check(),
//             "Run",
//             code_block("bash", "gico init", "inlinecode"),
//             "in the folder with several repos and it will
//             generate git-compose.yaml for you"
//         ],
//         p![
//             green_check(),
//             "With url argument like this:",
//             code_block("bash", "gico init <url>", "inlinecode"),
//             "will download git-compose.yaml file from external sources and clone
//             all the repos. It's handy, when you want to give a command
//             for a new developers to quickly rollout dev environment."
//         ]
//     ],
//     div![
//         C!["feature-section-right"],
//         img![
//             C!["image-w100"],
//             attrs! {
//                 At::Src => "static/images/gico-init.gif",
//             }
//         ],
//     ],
// ],
