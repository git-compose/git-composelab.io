// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::release::*;
use seed::{prelude::*};

#[derive(Clone)]
pub enum Msg {
    UrlChanged(subs::UrlChanged),
    // Redirect(String),
    ReleaseReceived(Release),
}
