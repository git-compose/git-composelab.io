#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::code_block::*;
use crate::git_compose_site::*;
use seed::{prelude::*, *};
mod feature;
mod hooks;
mod init;
mod vscode;
mod remotes;

pub fn home() -> Node<Msg> {
    div![
        main![div! {
                C!["content-grid", "centered-container"],
                div![
                    C!["content-left"],
                    p![
                    "CLI tool",
                    code_block("bash", "gico", "inlinecode"),
                    "will help you manage several local ",
                    a![
                        "Git",
                        attrs! {
                            At::Href => "https://git-scm.com/",
                        }
                    ],
                    " repositories. It is designed for use in multirepository
                    projects, where one team works on a few services
                    and/or libraries often making changes in several of them.",
                    ],
                    p![
                    "You can use",
                    code_block("bash", "gico up", "inlinecode"),
                    "to clone or update all repositories at once.
                    The idea is similar to docker-compose tool."
                    ]
                ],
                div![
                    C!["content-right"],
                    div![
                        C!["code-header"],
                        "Example ",
                        strong! [
                            "git-compose.yaml"
                        ],
                        " file"
                    ],
                    code_block("yaml", r#"version: "0.1"
repos:
  git-compose: 
    remote: https://gitlab.com/git-compose/git-compose
  git-compose-debian: 
    remote: https://gitlab.com/git-compose/git-compose-debian.git
  git-compose-vscode: 
    remote: https://gitlab.com/git-compose/git-compose-vscode
  git-compose-docs: 
    remote: https://gitlab.com/git-compose/git-compose-docs"#,
        "code-panel",
                ),
                ],
            }],
        hooks::render(),
        init::render(),
        vscode::render(),
        remotes::render()
    ]
}
