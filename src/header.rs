#![allow(clippy::wildcard_imports)]
// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.


use crate::git_compose_site::*;
use crate::release::*;
use seed::{prelude::*, *};

use seed_icons::fa::brands::gitlab;
use seed_icons::fa::solid::book;
use seed_icons::fa::solid::box_open;

use seed_icons::fa::solid::download;
use seed_icons::fa::solid::home as home_icon;
use seed_icons::fa::solid::info_circle;

pub fn header(title: String, release: Option<Release>) -> Node<Msg> {
    header![div![
        C!["header", "centered-container", "content-grid"],
        h1![
            C!["content-left"],
            img![attrs! {
                At::Height => "80px",
                At::Src => "/static/images/logo.svg",
            }],
            span![
                C!["header-text"],
                attrs! {
                    At::Height => "60px",
                },
                "Compose for Git / ",
                title
            ],
        ],
        div![
            C!["content-right", "header-links"],
            h3![
                a![
                    C!["header-link"],
                    home_icon::i_c(vec!["prime-color", "inline-icon"]),
                    "Features",
                    attrs! {
                        At::Href => "/",
                    },
                    // ev(Ev::Click, |_| Msg::Redirect(String::from("home")))
                ],
                a![
                    C!["header-link"],
                    info_circle::i_c(vec!["prime-color", "inline-icon"]),
                    "About",
                    attrs! {
                        At::Href => "/about",
                    },
                    // ev(Ev::Click, |_| Msg::Redirect(String::from("about")))
                ],
                match release {
                    Some(rel) => a![
                        C!["header-link"],
                        download::i_c(vec!["prime-color", "inline-icon"]),
                        format!("Install {}", rel.tag_name),
                        sup![
                            C!["tooltip"],
                            "alpha",
                            span![C!["tooltiptext"], "Currently in active development"],
                        ],
                        attrs! {
                            At::Href => "/install",
                        },
                        // ev(Ev::Click, |_| Msg::Redirect(String::from("install")))
                    ],
                    None => div![],
                }
            ],
            h4![
                a![
                    C!["header-link"],
                    book::i_c(vec!["prime-color", "inline-icon"]),
                    "Documentation",
                    attrs! {
                        At::Href => "https://git-compose.gitlab.io/git-compose-docs/",
                    }
                ],
                a![
                    C!["header-link"],
                    box_open::i_c(vec!["prime-color", "inline-icon"]),
                    "Releases",
                    attrs! {
                        At::Href => "https://gitlab.com/git-compose/git-compose/-/releases",
                    }
                ],
                a![
                    C!["header-link"],
                    gitlab::i_c(vec!["prime-color", "inline-icon"]),
                    "Sources",
                    attrs! {
                        At::Href => "https://gitlab.com/git-compose/",
                    }
                ],
            ]
        ],
    ]]
}
